<?php


if (MOBILE == 'SIM') {
    if(Util::mobile_device_detect(true,true,true,true,true,true,Util::caminho_projeto().'/mobile/',false))
    {
        header('Location: '.Util::caminho_projeto().'/mobile/');
    };
}




//  ESCOLHO AS CLASSES DO MENU
switch (Util::nome_diretorio())
{
    case PASTA_PROJETO;
        $class_home = "active";
    break;

    case PASTA_PROJETO."/a-casa";
        $class_casa = "active";
    break;

    case PASTA_PROJETO."/nosso-espaco";
        $class_espaco = "active";
    break;

    case PASTA_PROJETO."/promocoes";
        $class_promocoes = "active";
    break;

    case PASTA_PROJETO."/parceiros";
        $class_parceiros = "active";
    break;

    case PASTA_PROJETO."/lojas";
        $class_lojas = "active";
    break;

    case PASTA_PROJETO."/orcamentos";
        $class_orcamentos = "active";
    break;

    case PASTA_PROJETO."/fale-conosco";
        $class_fale_conosco = "active";
    break;

    default;
        $class_home = "active";
    break;

}

$result = $obj_site->select("tb_configuracoes","AND idconfiguracao = 1");
$config = mysql_fetch_array($result);
?>


<title><?php Util::imprime($obj_site->get_title($titulo_pagina)); ?></title>
<meta name="description" content="<?php Util::imprime($obj_site->get_description($description)); ?>">
<meta name="keywords" content="<?php Util::imprime($obj_site->get_keywords($keywords)); ?>">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=1200px">
<meta name="revisit-after" content="7 Days">
<meta name="robots" content="all">
<meta name="rating" content="general">
<meta name="author" content="masmidia.com.br">

<link rel="icon" href="<?php echo Util::caminho_projeto() ?>/imgs/favicon.ico">

<!-- Bootstrap core CSS -->
<link href="<?php echo Util::caminho_projeto(); ?>/dist/css/bootstrap.min.css" rel="stylesheet">


<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->


<link href="<?php echo Util::caminho_projeto(); ?>/css/style.css" rel="stylesheet" type="text/css" media="screen" />




<!-- Custom styles for this template -->
<link href="<?php echo Util::caminho_projeto() ?>/dist/css/non-responsive.css" rel="stylesheet">

<script src="<?php echo Util::caminho_projeto() ?>/jquery/jquery-3.0.0.min.js"></script>



<!--    ==============================================================  -->
<!--    ROYAL SLIDER    -->
<!--    ==============================================================  -->

<!-- slider JS files -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>






<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/ie10-viewport-bug-workaround.js"></script>






<!-- Bootstrap Validator
https://github.com/nghuuphuoc/bootstrapvalidator
================================================== -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/bootstrapValidator.min.css"/>
 <!-- Include FontAwesome CSS if you want to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/font-awesome.min.css"/>


<!-- Either use the compressed version (recommended in the production site) -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrapValidator.min.js"></script>

<!-- Or use the original one with all validators included -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrapValidator.js"></script>

<!-- language -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/src/js/language/pt_BR.js"></script>


<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/bootstrap-lightbox.min.css">
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/bootstrap-lightbox.min.js"></script>



<!-- The paths might be changed to suit with your folder structure -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/dist/css/intlTelInput.css" />
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/intlTelInput.min.js"></script>















<?php /*

<!-- Rating
http://plugins.krajee.com/star-rating
================================================== -->
<link href="<?php echo Util::caminho_projeto() ?>/dist//css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo Util::caminho_projeto() ?>/dist//js/star-rating.min.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function () {

    $('.avaliacao').rating({
          min: 0,
          max: 5,
          step: 1,
          size: 'xs',
          showClear: false,
          disabled: true,
          clearCaption: 'Seja o primeiro a avaliar.',
          starCaptions: {
                            0.5: 'Half Star',
                            1: 'Ruim',
                            1.5: 'One & Half Star',
                            2: 'Regular',
                            2.5: 'Two & Half Stars',
                            3: 'Bom',
                            3.5: 'Three & Half Stars',
                            4: 'Ótimo',
                            4.5: 'Four & Half Stars',
                            5: 'Excelente'
                        }
       });


});
</script>

*/ ?>




<script>
    $('#myAffix').affix({
      offset: {
        top: 100,
        bottom: function () {
          return (this.bottom = $('.footer').outerHeight(true))
        }
      }
    })
</script>








<!--    ====================================================================================================     -->
<!--    ADICIONA PRODUTO AOO CARRINHO    -->
<!--    ====================================================================================================     -->
<script type="text/javascript">
function add_solicitacao(id, tipo_orcamento)
{
    window.location = '<?php echo Util::caminho_projeto() ?>/add_prod_solicitacao.php?id='+id+'&tipo_orcamento='+tipo_orcamento;
}
</script>





<!-- FlexSlider -->
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
<script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

<!-- Optional FlexSlider Additions -->
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
<script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
<script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


<!-- Syntax Highlighter -->
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
<link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


<!-- Demo CSS -->
<link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />






<?php /*

  <!-- ---- LAYER SLIDER ---- -->
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/touchcarousel.css"/>
    <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/black-and-white-skin/black-and-white-skin.css" />
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/touchcarousel/jquery.touchcarousel-1.2.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $("#carousel-gallery").touchCarousel({
            itemsPerPage: 1,
            scrollbar: true,
            scrollbarAutoHide: true,
            scrollbarTheme: "dark",
            pagingNav: false,
            snapToItems: true,
            scrollToLast: false,
            useWebkit3d: true,
            loopItems: true,
            autoplay: true
        });
    });
    </script>
    <!-- XXXX LAYER SLIDER XXXX -->


*/ ?>

<!--    ====================================================================================================     -->
<!--    CLASSE DE FONTS DO SITE http://fortawesome.github.io/Font-Awesome/icons/    -->
<!--    ====================================================================================================     -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">







<link href="https://file.myfontastic.com/eTsTzZaRSpWVkWA4EKLf2d/icons.css" rel="stylesheet">





<!-- usando carroucel 01 -->
  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel-categorias').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 160,
        itemMargin: 1,
        asNavFor: '#slider'
      });

      $('#slider-categorias').flexslider({
        animation: "slide",
        controlNav: false,
        itemWidth: 140,
        itemMargin: 0,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>





<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->
<!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/bootstrap-select/dist/css/bootstrap-select.min.css">

  <!-- Latest compiled and minified JavaScript -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/bootstrap-select/dist/js/bootstrap-select.min.js"></script>


  <script type="text/javascript">
    $('.selectpicker').selectpicker({
      style: 'btn-info',
      size: 4
    });

  </script>
<!--  ==============================================================  -->
<!-- SELECT BOOSTRAP-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- DIRECIONA SELECT    -->
<!-- ======================================================================= -->
<script>
  $(function(){
      // bind change event to select
      $('.submit-select').bind('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });
</script>
<!-- ======================================================================= -->
<!-- DIRECIONA SELECT    -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!--PRODUTO HOVER MOSTRANDO CARRINHO E SAIBA MAIS    -->
<!-- ======================================================================= -->

<script type="text/javascript">
$(document).ready(function() {

  $(".produto-hover").hover(
    function () {
      $(this).stop().animate({opacity:1});
    },
    function () {
      $(this).stop().animate({opacity:0});
    }
    );

});
</script>
<!-- ======================================================================= -->
<!--PRODUTO HOVER MOSTRANDO CARRINHO E SAIBA MAIS    -->
<!-- ======================================================================= -->


<!--  ==============================================================  -->
<!-- SCROLL-->
<!--  ==============================================================  -->
<script type = "text/javascript">
      $(function () {
          $('#scrollToBottom').bind("click", function () {
              $('html, body').animate({ scrollTop: $(document).height() }, 4000);
              return false;
          });
          // $('#scrollToTop').bind("click", function () {
          //     $('html, body').animate({ scrollTop: 0 }, 1200);
          //     return false;
          // });
      });
  </script>
  <!--  ==============================================================  -->
  <!-- SCROLL-->
  <!--  ==============================================================  -->








<script type="text/javascript">
  $(document).ready(function(){
      $('#topo_tipos_veiculos').change(function(){
          $('#topo_marca_veiculos').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_marcas_veiculos_url_amigavel.php?id='+$('#topo_tipos_veiculos').val() );
      });
  });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#topo_marca_veiculos').change(function(){
            $('#topo_modelo_veiculos').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_modelos_veiculos_url_amigavel.php?id='+$('#topo_marca_veiculos').val()+'&tipo_veiculo='+$('#topo_tipos_veiculos').val() );
        });
    });
</script>


<script type="text/javascript">
    $(document).ready(function(){
        $('#topo_modelo_veiculos').change(function(){
            $('#busca_topo_ano').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_ano.php');
        });
    });
</script>









