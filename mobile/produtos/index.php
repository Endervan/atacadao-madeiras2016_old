
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 112px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">
      <div class="col-xs-6">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb">
          	<a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
          	<a class="active">Produtos</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

       <div class="col-xs-6 empresa_descricao top10 text-right">
        <h1>PRODUTOS</h1>
       </div>

  </div>
</div>





<!-- ======================================================================= -->
<!-- TIPOS DE PRODUTOS DESCRICAO    -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row produtos_geral">


    <!-- ======================================================================= -->
    <!-- ITEM 01   -->
    <!-- ======================================================================= -->
    <div class="top30"></div>
    <div class="col-xs-6">
      <a href="<?php echo Util:: caminho_projeto() ?>/mobile/produto">
        <img src="<?php echo Util:: caminho_projeto() ?>/mobile/imgs/produtos01.jpg" alt="" />
      </a>
    </div>
    <div class="col-xs-6">
      <a href="<?php echo Util:: caminho_projeto() ?>/mobile/produto">
        <h1>Telhas</h1>
        <div class="top5">
          <p>
            Disponibilizamos para nossos clie
              ntes as mais diversas madeiras, le
              galizadas e com certificado de ext
              ração.   Veja abaixo alguns dos tip
              os de madeiras que nós comercia
              lizamos:     *Peroba vermelha *Ce
              drinho *Pinus *Angelim ...
          </p>
        </div>
        </a>
    </div>
    <div class="clearfix"></div>
    <div class="borda_cinza top15"></div>
    <!-- ======================================================================= -->
    <!-- ITEM 01   -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- ITEM 01   -->
    <!-- ======================================================================= -->
    <div class="top30"></div>
    <div class="col-xs-6">
      <a href="<?php echo Util:: caminho_projeto() ?>/mobile/produto">
        <img src="<?php echo Util:: caminho_projeto() ?>/mobile/imgs/produtos01.jpg" alt="" />
      </a>
    </div>
    <div class="col-xs-6">
      <a href="<?php echo Util:: caminho_projeto() ?>/mobile/produto">
        <h1>Telhas</h1>
        <div class="top5">
          <p>
            Disponibilizamos para nossos clie
              ntes as mais diversas madeiras, le
              galizadas e com certificado de ext
              ração.   Veja abaixo alguns dos tip
              os de madeiras que nós comercia
              lizamos:     *Peroba vermelha *Ce
              drinho *Pinus *Angelim ...
          </p>
        </div>
        </a>
    </div>
    <div class="clearfix"></div>
    <div class="borda_cinza top15"></div>
    <!-- ======================================================================= -->
    <!-- ITEM 01   -->
    <!-- ======================================================================= -->


  </div>
</div>
<!-- ======================================================================= -->
<!-- TIPOS DE PRODUTOS DESCRICAO    -->
<!-- ======================================================================= -->



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
