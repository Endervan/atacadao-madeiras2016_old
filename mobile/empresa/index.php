
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 112px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">
      <div class="col-xs-6">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb">
          	<a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
          	<a class="active">Empresa</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

       <div class="col-xs-6 empresa_descricao top10 text-right">
        <h1>A EMPRESA</h1>
       </div>

       <div class="col-xs-12 top30">
          <img src="<?php echo Util:: caminho_projeto() ?>/mobile/imgs/empresa01.jpg" alt="" />
       </div>

       <div class="clearfix"></div>

     <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
       <div class="col-xs-12 top30 empresa_descricao_geral">
          <p>
            <?php Util::imprime($row[descricao]); ?>
          </p>

       </div>
  </div>
</div>




<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
