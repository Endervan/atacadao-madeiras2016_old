
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 112px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">
      <div class="col-xs-6">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb">
          	<a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
          	<a class="active">Contatos</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

       <div class="col-xs-6 empresa_descricao top10 text-right">
        <h1>CONTATOS</h1>
       </div>

  </div>
</div>



<div class="container">
  <div class="row">
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 top25 contatos">

      <div class="media">

        <div class="media-body">
          <h1 class="right5">
            <img class="right5" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone_preto.png" alt="" />
            <?php Util::imprime($config[ddd1]); ?>
            <?php Util::imprime($config[telefone1]); ?>
          </h1>
        </div>

        <div class="media-left media-middle">
          <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_contatos1.png" alt="" />
          </a>
        </div>

      </div>

    </div>
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->

  </div>
</div>


  <div class="container">
    <div class="row produtos_destaques">

      <div class="col-xs-12 top950 padding0">

        <?php
            //  VERIFICO SE E PARA ENVIAR O EMAIL
            if(isset($_POST[nome]))
            {
               $texto_mensagem = "
                                  Nome: ".($_POST[nome])." <br />
                                  Assunto: ".($_POST[assunto])." <br />
                                  Telefone: ".($_POST[telefone])." <br />
                                  Email: ".($_POST[email])." <br />
                                  Mensagem: <br />
                                  ".(nl2br($_POST[mensagem]))."
                                  ";
                Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
                Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

                Util::alert_bootstrap("Obrigado por entrar em contato.");
                unset($_POST);
            }

            ?>

        <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
          <div class="fundo_formulario_fale">
            <!-- formulario orcamento -->

            <div class="col-xs-6 top15">
              <div class="form-group input100 has-feedback">
                <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                <span class="fa fa-user form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6 top15">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="email" class="form-control fundo-form1 input-lg input100" placeholder="E-MAIL">
                <span class="fa fa-envelope form-control-feedback top15"></span>
              </div>
            </div>

            <div class="clearfix">  </div>

            <div class="col-xs-6 top15">
              <div class="form-group  input100 has-feedback">
                <input type="tel" name="telefone" class="form-control fundo-form1 input-lg input100" placeholder="TELEFONE">
                <span class="fa fa-phone form-control-feedback top15"></span>
              </div>
            </div>

            <div class="col-xs-6 top15">
              <div class="form-group  input100 has-feedback">
                <input type="text" name="assunto" class="form-control fundo-form1 input-lg input100" placeholder="ASSUNTO">
                <span class="fa fa-star form-control-feedback"></span>
              </div>
            </div>

            <div class="clearfix">  </div>


            <div class="col-xs-12 top15">
             <div class="form-group input100 has-feedback">
              <textarea name="mensagem" cols="30" rows="8" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
              <span class="fa fa-pencil form-control-feedback top15"></span>
            </div>
          </div>


          <!-- formulario orcamento -->
          <div class="col-xs-12 text-right">
            <div class="top15 bottom25">
              <button type="submit" class="btn btn-formulario" name="btn_contato">
                ENVIAR
              </button>
            </div>
          </div>

        </div>

        <!--  ==============================================================  -->
        <!-- FORMULARIO-->
        <!--  ==============================================================  -->
      </form>

    </div>
  </div>
</div>



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>




<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
       telefone: {
         validators: {
           phone: {
               country: 'BR',
               message: 'Informe um telefone válido.'
           }
         },
       }
    }
  });
  });
</script>
