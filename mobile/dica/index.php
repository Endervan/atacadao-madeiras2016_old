
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 10);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 112px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">
      <div class="col-xs-6">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb">
          	<a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
            <a href="<?php echo Util:: caminho_projeto() ?>/mobile/dicas">Dicas</a>
          	<a class="active">Dica</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

       <div class="col-xs-6 empresa_descricao top10 text-right">
        <h1>DICA</h1>
       </div>

  </div>
</div>


<!-- ======================================================================= -->
<!-- DICA DENTRO DESCRICAO    -->
<!-- ======================================================================= -->
<div class="container">
  <div class="row">
    <div class="col-xs-12 top25">
        <h2>Opções em assoalho de madeira.</h2>
        <div class="borda_cinza top5">  </div>

          <div class="top15">
            <img src="<?php echo Util:: caminho_projeto() ?>/mobile/imgs/dica_dentro01.jpg" alt="" />
           </div>

           <div class="top40">
              <p>
                A Atacadão da Madeira é uma empresa séria e pioneira no ra
              mo de madeiras, estamos atuando no mercado de madeiras
              ha 16 anos. Destacamos pelos produtos de altíssima qualida
              de e uma política de preços altamente competitiva em rela
              ção ao mercado.

              Procuramos manter sempre em disponibilidade em nosso es
              toque o material anunciado ao clientes além dos lançamentos
               feitos por nossos fornecedores. Para assim atender melhor
              ao cliente nas suas necessidades imediatas.
              </p>
           </div>

    </div>

  </div>
</div>
<!-- ======================================================================= -->
<!-- DICA DENTRO DESCRICAO    -->
<!-- ======================================================================= -->



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
