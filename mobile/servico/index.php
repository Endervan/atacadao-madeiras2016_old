
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 8) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 112px center  no-repeat;
  }
</style>

<body class="bg-interna">


	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <div class="container">
    <div class="row top25">
      <div class="col-xs-7">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb">
          	<a href="<?php echo Util:: caminho_projeto() ?>/mobile/"><i class="fa fa-home"></i></a>
            <a href="<?php echo Util:: caminho_projeto() ?>/mobile/">Serviços</a>
          	<a class="active">Serviço</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>

       <div class="col-xs-5 empresa_descricao top10 text-right">
        <h1>SERVIÇOS</h1>
       </div>

  </div>
</div>



<div class="container">
  <div class="row produtos_dentro top20">
    <div class="col-xs-6 top15">
        <h1>Madeiras</h1>
    </div>

    <div class="col-xs-6">
      <a href="<?php echo Util:: caminho_projeto() ?>/mobile/orcamentos" class="btn btn_transparente_branco1 col-xs-12" title="SOLICITAR UM ORÇAMENTO">
          SOLICITAR UM ORÇAMENTO
        </a>
    </div>

    <div class="col-xs-12">
     <div class="borda_cinza top5">  </div>
    </div>
    <!--  ==============================================================  -->
    <!--  CARROUCEL PRODUTOS-->
    <!--  ==============================================================  -->
    <div class="col-xs-12 carroucel-produtos top15">
     <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators navs_produtos_home">
          <?php
          $result = $obj_site->select("tb_banners", "and tipo_banner = 4 ");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while ($row = mysql_fetch_array($result)) {
              $imagens[] = $row;
              ?>
              <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
              <?php
              $i++;
            }
          }
          ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <?php
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">

                <?php if (!empty($imagem[url])): ?>
                  <a href="<?php Util::imprime($imagem[url]); ?>" >
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                  </a>
                <?php else: ?>
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                <?php endif; ?>

              </div>
              <?php
              $i++;
            }
          }
          ?>
         </div>


       </div>
      </div>
      <!--  ==============================================================  -->
      <!--  CARROUCEL PRODUTOS-->
      <!--  ==============================================================  -->

      <div class="col-xs-12 top35">
        <p>
          A Atacadão da Madeira é uma empresa séria e pioneira no ra
          mo de madeiras, estamos atuando no mercado de madeiras
          ha 16 anos. Destacamos pelos produtos de altíssima qualida
          de e uma política de preços altamente competitiva em rela
          ção ao mercado.

          Procuramos manter sempre em disponibilidade em nosso es
          toque o material anunciado ao clientes além dos lançamentos
           feitos por nossos fornecedores. Para assim atender melhor
          ao cliente nas suas necessidades imediatas.
        </p>
      </div>
  </div>
</div>







<?php require_once('../includes/rodape.php'); ?>

</body>

</html>
