<div class="container">
  <div class="row menu_topo">


    <!-- ======================================================================= -->
    <!-- LOGO -->
    <!-- ======================================================================= -->
    <div class="text-center logo">
          <a href="<?php echo Util::caminho_projeto() ?>/mobile" title="HOME">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
          </a>
    </div>
    <!-- ======================================================================= -->
    <!-- LOGO -->
    <!-- ======================================================================= -->

    <!-- ======================================================================= -->
    <!-- MENU  -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 bottom5">
      <select name="menu" id="menu-site" class="select-menu top5 pull-right">
        <option value=""></option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVIÇOS</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</option>
        <option value="<?php echo Util::caminho_projeto() ?>/mobile/contatos">CONTATOS</option>
      </select>
    </div>
    <!-- ======================================================================= -->
    <!-- MENU  -->
    <!-- ======================================================================= -->


    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->
    <div class="col-xs-12 fundo_preto pt10 pb10">

      <div class="media pull-right">

        <div class="media-body">
          <h4 class="right5">
            <img class="right5" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_phone.png" alt="" />
            <?php Util::imprime($config[ddd1]); ?>
            <?php Util::imprime($config[telefone1]); ?>
          </h4>
        </div>

        <div class="media-left media-middle">
          <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
            <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_contatos.png" alt="" />
          </a>
        </div>

      </div>

    </div>
    <!-- ======================================================================= -->
    <!-- CONTATOS  -->
    <!-- ======================================================================= -->

  </div>
</div>
