<h4 class="titulo-linha-baixo">ATENDIMENTO</h4>
            <h4 class="top35"><i class="fa fa-phone" aria-hidden="true"></i> <?php Util::imprime($config[telefone1]); ?></h4>

            <?php if (!empty($config[telefone2])): ?>
                <h4 class="top25"><i class="fa fa-phone" aria-hidden="true"></i> <?php Util::imprime($config[telefone2]); ?></h4>
            <?php endif ?>

            <?php if (!empty($config[telefone3])): ?>
                <h4 class="top25"><i class="fa fa-phone" aria-hidden="true"></i> <?php Util::imprime($config[telefone3]); ?></h4>
            <?php endif ?>

            <?php if (!empty($config[telefone4])): ?>
                <h4 class="top25"><i class="fa fa-phone" aria-hidden="true"></i> <?php Util::imprime($config[telefone4]); ?></h4>
            <?php endif ?>

            
            <h4 class="titulo-linha-baixo top50">ONDE ESTAMOS</h4>

            <div class="text-center top30">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-location.jpg" alt="">
            </div>

            <p class="top10"><?php Util::imprime($config[endereco]); ?></p>