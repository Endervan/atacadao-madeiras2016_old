<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- slider JS files -->
  <script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>
  <script>
    jQuery(document).ready(function($) {
      // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
      // it's recommended to disable them when using autoHeight module
      $('#content-slider-1').royalSlider({
        autoHeight: true,
        arrowsNav: true,
        fadeinLoadedSlide: false,
        controlNavigationSpacing: 0,
        controlNavigation: 'tabs',
        imageScaleMode: 'none',
        imageAlignCenter: false,
        loop: false,
        loopRewind: true,
        numImagesToPreload: 6,
        keyboardNavEnabled: true,
        usePreloader: false,
        autoPlay: {
              // autoplay options go gere
              enabled: true,
              pauseOnHover: true
            }

          });
    });
  </script>


</head>
<body>


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


<?php /*

  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container-fluid">
    <div class="row subir_banner">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 3");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->






<div class="container-fluid bg_azul">
  <div class="row">
    <div class="container">
      <div class="row  subir">




        <!-- ======================================================================= -->
        <!-- CONTATOS  -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/contatos.php'); ?>
        <!-- ======================================================================= -->
        <!-- CONTATOS  -->
        <!-- ======================================================================= -->



        <!-- ======================================================================= -->
        <!-- MENU CATEGORIAS    -->
        <!-- ======================================================================= -->
        <div class="col-xs-12 bg_branco pt20 pb20 text-center">
          <!-- item 01 -->
            <div class="col-xs-4 bateria_carro">
              <a href="<?php echo Util::caminho_projeto() ?>/">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_carro.png" alt="" />
                <div class="top5">
                  <h1>BATERIAS PARA CARROS BATERIAS PARA CARROS</h1>
                </div>
                </a>
                <div class="top25 bottom25">
                    <h2><span>120</span>PRODUTOS DISPONÍVEIS</h2>
                </div>
            </div>


            <!-- item 01 -->
              <div class="col-xs-4 bateria_caminhao">
                <a href="<?php echo Util::caminho_projeto() ?>/">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_caminhao.png" alt="" />
                  <div class="top5">
                    <h1>BATERIAS PARA CARROS</h1>
                  </div>
                  </a>
                  <div class="top25 bottom25">
                      <h2><span>1200</span>PRODUTOS DISPONÍVEIS</h2>
                  </div>
              </div>

              <!-- item 01 -->
                <div class="col-xs-4 bateria_moto">
                  <a href="<?php echo Util::caminho_projeto() ?>/">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_moto.png" alt="" />
                    <div class="top5">
                      <h1>BATERIAS PARA CARROS</h1>
                    </div>
                    </a>
                    <div class="top25 bottom25">
                        <h2><span>1200</span>PRODUTOS DISPONÍVEIS</h2>
                    </div>
                </div>

                <!-- item 01 -->
                  <div class="col-xs-4 bateria_barco">
                    <a href="<?php echo Util::caminho_projeto() ?>/">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_barco.png" alt="" />
                      <div class="top5">
                        <h1>BATERIAS PARA CARROS</h1>
                      </div>
                      </a>
                      <div class="top25 bottom25">
                          <h2><span>1200</span>PRODUTOS DISPONÍVEIS</h2>
                      </div>
                  </div>

                  <!-- item 01 -->
                    <div class="col-xs-4 bateria_lampada">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_lampada.png" alt="" />

                        <div class="top5">
                          <h1>BATERIAS PARA CARROS</h1>
                        </div>
                        </a>
                        <div class="top25 bottom25">
                            <h2><span>1200</span>PRODUTOS DISPONÍVEIS</h2>
                        </div>
                    </div>

                    <!-- item 01 -->
                      <div class="col-xs-4 bateria_baterias">
                        <a href="<?php echo Util::caminho_projeto() ?>/">
                          <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_baterias.png" alt="" />
                          <div class="top5">
                            <h1>BATERIAS PARA CARROS</h1>
                          </div>
                          </a>
                          <div class="top25 bottom25">
                              <h2><span>1200</span>PRODUTOS DISPONÍVEIS</h2>
                          </div>
                      </div>


                </div>
                <!-- ======================================================================= -->
                <!-- MENU CATEGORIAS    -->
                <!-- ======================================================================= -->
              </div>
            </div>
          </div>
        </div>




        <!--  ==============================================================  -->
        <!-- MENU-->
        <!--  ==============================================================  -->
        <div class="container">
         <div class="row top30">
           <div class="col-xs-12 ">
            <div class="menu_produtos_home">

              <div class="col-xs-9 top15 padding0">
               <ul class="bottom10">
                 <li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
                   <a href="<?php echo Util::caminho_projeto() ?>/produtos">EM DESTAQUES</a>
                 </li>

                 <li class="<?php if(Url::getURL( 0 ) == "a-unaflor"){ echo "active"; } ?>">
                   <a href="<?php echo Util::caminho_projeto() ?>/produtos">MAIS VENDIDOS</a>
                 </li>

                 <li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>">
                   <a href="<?php echo Util::caminho_projeto() ?>/produtos">NOVOS PRODUTOS</a>
                 </li>
               </ul>
             </div>


             <!--  ==============================================================  -->
             <!-- VER TODOS-->
             <!--  ==============================================================  -->
             <div class="col-xs-3 padding0 text-right">
               <a class="btn btn_ver_todos" href="<?php echo Util::caminho_projeto() ?>/produtos" title="VER TODOS OS PRODUTOS">
                 VER TODOS OS PRODUTOS
               </a>
             </div>
             <!--  ==============================================================  -->
             <!-- VER TODOS-->
             <!--  ==============================================================  -->
           </div>

         </div>
       </div>
     </div>
     <!--  ==============================================================  -->
     <!-- MENU-->
     <!--  ==============================================================  -->



     <!--  ==============================================================  -->
     <!-- produto home-->
     <!--  ==============================================================  -->
     <div class="container">
       <div class="row top35">

         <?php
         $result = $obj_site->select("tb_produtos", "order by rand() limit 8");
         if (mysql_num_rows($result) > 0) {
           $i = 0;
           while ($row = mysql_fetch_array($result)) {
           ?>
             <div class="col-xs-3">
               <div class="thumbnail produtos-home">

                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 271, 218, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>

                <div class="produto-hover">
                  <div class="col-xs-6 text-center">
                    <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba mais">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-saiba-mais.png" alt="" class="input100">
                    </a>
                  </div>
                  <div class="col-xs-6 hover-btn-orcamento">
                    <a href="javascript:void(0);" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                      <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-adicionar-orcamento.png" alt="" class="input100">
                    </a>
                  </div>

                </div>

                <!--  ==============================================================  -->
                <!-- TEXT DIAGONAL-->
                <!--  ==============================================================  -->
                <div class="diagonal">
                  <h3><?php Util::imprime($row[anos]); ?> ANOS</h3>
                </div>
                <!--  ==============================================================  -->
                <!-- TEXT DIAGONAL-->
                <!--  ==============================================================  -->

                <div class="col-xs-12 padding0">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/barra_star_produto.png" alt="" />
                </div>


                <div class="caption top10">
                  <h1><?php Util::imprime($row[titulo]); ?></h1>
                    <div class="top10">
                    <p><?php Util::imprime($row[descricao],500); ?></p>
                    <div class="top15">
                        <h2>Tipo de Polo: <?php Util::imprime($row[codigo]); ?></h2>
                    </div>
                  </div>
                </div>
              </div>

            </div>
           <?php
             if($i == 3){
               echo '<div class="clearfix"></div>';
               $i = 0;
             }else{
               $i++;
             }

           }
         }
         ?>

       </div>
     </div>
     <!--  ==============================================================  -->
     <!-- produto home-->
     <!--  ==============================================================  -->


    <div class="container-fluid bg_conhecar_mais top60">
     <div class="row">
        <div class="container">
          <div class="row">
            <!--  ==============================================================  -->
            <!-- CONHECA MAIS-->
            <!--  ==============================================================  -->
            <div class="col-xs-12 top50">
                <h4>CONHEÇA O <b>SHOPPING DAS BATERIAS</b></h4>
            </div>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
            <div class="col-xs-8 saiba_descricao top30">
            <p>
              <?php Util::imprime($row[descricao],500); ?>
            </p>

          </div>
          <div class="col-xs-12 top25 bottom25">
            <a class="btn btn_transparente " href="<?php echo Util::caminho_projeto() ?>/empresa/"
              title="CONHEÇA MAIS NOSSA EMPRESA">
              SAIBA MAIS
            </a>

          </div>
          <!--  ==============================================================  -->
          <!-- CONHECA MAIS-->
          <!--  ==============================================================  -->

        <div class="clearfix"></div>


          <!--  ==============================================================  -->
          <!-- NOSSOS  SERVICOS-->
          <!--  ==============================================================  -->
          <div class="bg_servicos top150">

        <?php
        $result = $obj_site->select("tb_servicos", "order by rand() limit 4");
        if (mysql_num_rows($result) > 0) {
          $i = 0;
          while($row = mysql_fetch_array($result)){
            ?>

          <div class="col-xs-6">
            <div class="media top40">
              <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <div class="media-left media-middle">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 56, 49, array("class"=>"media-object", "alt"=>"$row[imagem]")) ?>
              </div>
              <div class="media-body">
                <h1 class="media-heading"><?php Util::imprime($row[titulo]); ?></h1>
                <div class="top10">
                  <p>
                    <?php Util::imprime($row[descricao], 200); ?>
                  </p>
                </div>
              </div>
              </a>
            </div>
          </div>

          <?php
          if ($i == 1) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>

          <!--  ==============================================================  -->
          <!-- NOSSOS  SERVICOS-->
          <!--  ==============================================================  -->



            <div class="clearfix"></div>
              <div class="top105"></div>
          <!--  ==============================================================  -->
            <!--NOSSAS MARCAS-->
          <!--  ==============================================================  -->
          <div class="col-xs-2">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas01.png" alt="" />
            </a>
          </div>

          <div class="col-xs-2">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas02.png" alt="" />
            </a>
          </div>

          <div class="col-xs-2">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas03.png" alt="" />
            </a>
          </div>

          <div class="col-xs-2">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas04.png" alt="" />
            </a>
          </div>

          <div class="col-xs-2">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas02.png" alt="" />
            </a>
          </div>

          <div class="col-xs-2">
            <a href="<?php echo Util::caminho_projeto() ?>/servicos">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas03.png" alt="" />
            </a>
          </div>
          <!--  ==============================================================  -->
            <!--NOSSAS MARCAS-->
          <!--  ==============================================================  -->

          </div>
          <!--  ==============================================================  -->
          <!-- NOSSOS  SERVICOS-->
          <!--  ==============================================================  -->



        </div>
      </div>
    </div>
  </div>
     <!--  ==============================================================  -->
     <!-- CONHECA MAIS-->
     <!--  ==============================================================  -->

*/ ?>

      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->



    </body>

    </html>
