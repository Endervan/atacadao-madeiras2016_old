<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 148px center no-repeat;
    }
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->







  <div class="container">
    <div class="row top25">
      <div class="col-xs-8">
        <!-- ======================================================================= -->
        <!-- Breadcrumbs    -->
        <!-- ======================================================================= -->
          <div class="breadcrumb top15">
          	<a href="<?php echo Util:: caminho_projeto() ?>/"><i class="fa fa-home"></i></a>
          	<a class="active">Empresa</a>
            </div>
            <!-- ======================================================================= -->
            <!-- Breadcrumbs    -->
            <!-- ======================================================================= -->
      </div>


      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/contatos.php') ?>
      <!-- ======================================================================= -->
      <!-- CONTATOS  -->
      <!-- ======================================================================= -->

  </div>
 </div>


 <div class="container">
   <div class="row top170">
     <div class="col-xs-12 empresa_descricao">
      <h1><span>CONHEÇA O </span>SHOPPING DAS BATERIAS</h1>
     </div>

     <div class="clearfix"></div>

    <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
     <div class="col-xs-12 top105 bg_branco">
      <div class="pg20">
        <p>
          <?php Util::imprime($row[descricao]); ?>
        </p>
        </div>

        <div class="col-xs-6 padding0 top50">
          <div class="media">
            <div class="media-left media-middle">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_chegar.png" alt="">
            </div>
            <div class="media-body">
                <img class="media-heading" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_chegar_mais.png" alt="">
            </div>
          </div>
        </div>

        <div class="col-xs-12 padding0 bottom60">
          <div class="top40 bottom20">
            <p><?php Util::imprime($config[endereco]); ?></p>
          </div>
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="405" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

     </div>






     <!--  ==============================================================  -->
       <!--NOSSAS MARCAS-->
     <!--  ==============================================================  -->
     <div class="col-xs-2">
       <a href="<?php echo Util::caminho_projeto() ?>/servicos">
         <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas01.png" alt="" />
       </a>
     </div>

     <div class="col-xs-2">
       <a href="<?php echo Util::caminho_projeto() ?>/servicos">
         <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas02.png" alt="" />
       </a>
     </div>

     <div class="col-xs-2">
       <a href="<?php echo Util::caminho_projeto() ?>/servicos">
         <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas03.png" alt="" />
       </a>
     </div>

     <div class="col-xs-2">
       <a href="<?php echo Util::caminho_projeto() ?>/servicos">
         <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas04.png" alt="" />
       </a>
     </div>

     <div class="col-xs-2">
       <a href="<?php echo Util::caminho_projeto() ?>/servicos">
         <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas02.png" alt="" />
       </a>
     </div>

     <div class="col-xs-2">
       <a href="<?php echo Util::caminho_projeto() ?>/servicos">
         <img src="<?php echo Util::caminho_projeto() ?>/imgs/marcas03.png" alt="" />
       </a>
     </div>
     <!--  ==============================================================  -->
       <!--NOSSAS MARCAS-->
     <!--  ==============================================================  -->


 </div>
</div>











<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
