<?php
ob_start();
session_start();

class Modelo_Veiculo_Model extends Dao
{

	private $nome_tabela = "tb_modelos_veiculos";
	private $chave_tabela = "idmodeloveiculo";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
            $this->obj_imagem = new Imagem();
            parent::__construct();
	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){

            if(count($ordem) > 0):

                foreach($ordem as $key=>$orde):

                    $sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
                    parent::executaSQL($sql);

                endforeach;

            endif;

	}


	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
        $obj_jquery = new Biblioteca_Jquery();
        ?>

        <script type="text/javascript">
                $(document).ready(function(){
                    $('#id_tipoveiculo').change(function(){
                        $('#id_marcaveiculo').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_marcas_veiculos.php?id='+$('#id_tipoveiculo').val() );
                    });
                });
        </script>

       
        <div class="col-xs-6 form-group ">
            <label>Tipo de veículo</label>
            <?php Util::cria_select_bd("tb_tipos_veiculos", "idtipoveiculo", "titulo", "id_tipoveiculo", $dados[id_tipoveiculo], "form-control fundo-form1 input100") ?>
        </div>
        

        <div class="col-xs-6 form-group ">
            <label>Marca do veículo</label>

            <?php if(!empty($dados[id_marcaveiculo])): ?>
                <?php 
                $sql = "SELECT * FROM tb_marcas_veiculos WHERE id_tipoveiculo = '$dados[id_tipoveiculo]' ";
                Util::cria_select_bd_com_sql("id_marcaveiculo", $dados[id_marcaveiculo], $sql, "", "form-control fundo-form1 input100")
                ?>
            <?php else: ?>
                <select name="id_marcaveiculo" id="id_marcaveiculo" class="form-control fundo-form1 input100">
                  <option value="">Selecione</option>
                </select>
            <?php endif; ?>

        </div>
        

       
        <div class="col-xs-12 form-group ">
            <label>Título</label>
            <input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
        </div>





        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->
        <?php //require_once("../includes/google_seo.php"); ?>
        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->

        



        <!-- ======================================================================= -->
        <!-- VALIDACAO DO FORMULARIO    -->
        <!-- ======================================================================= -->
        <script>
          $(document).ready(function() {
            $('.FormPrincipal').bootstrapValidator({
              message: 'This value is not valid',
              feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
              fields: {


              titulo: {
                validators: {
                  notEmpty: {

                  }
                }
              }, 

              id_tipoveiculo: {
                validators: {
                  notEmpty: {

                  }
                }
              },  

              id_marcaveiculo: {
                validators: {
                  notEmpty: {

                  }
                }
              },  

              email: {
                validators: {
                  notEmpty: {

                  },
                  emailAddress: {
                    message: 'Esse endereço de email não é válido'
                  }
                }
              },
              
              mensagem: {
                validators: {
                  notEmpty: {

                  }
                }
              }
            }
          });
          });
        </script>        




            <?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_arquivo("../../uploads", $_FILES[imagem]);

                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 236, 212);
            }

            //	CADASTRA O USUARIO
            $id = parent::insert($this->nome_tabela, $dados);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "CADASTRO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);


            Util::script_msg("Cadastro efetuado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_arquivo("../../uploads", $_FILES[imagem]);


                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 236, 212);
            }

            parent::update($this->nome_tabela, $id, $dados);


            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "ALTERAÇÃO DO CLIENTE $dados[nome]", $sql, $_SESSION[login][idlogin]);



            Util::script_msg("Alterado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}




	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
            if($ativo == "SIM")
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
            else
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
            //	BUSCA OS DADOS
            $row = $this->select($id);

            $sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
            if($id != "")
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            WHERE
                                    " . $this->chave_tabela. " = '$id'
                            ";
                    return mysql_fetch_array(parent::executaSQL($sql));
            }
            else
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            ORDER BY
                                    ordem, id_tipoveiculo, id_marcaveiculo, titulo
                            ";
                    return parent::executaSQL($sql);
            }

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
            $obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
            $obj_imagem->redimensiona($largura, $altura, $tipo);
            $obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
