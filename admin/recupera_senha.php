<?php
require_once("../class/Include.class.php");
require_once("login/Login_Model.php");
$obj_login = new Login_Model();







?>
<!doctype html>
<html>
<head>
<?php require_once('./includes/head.php'); ?>






</head>

<body>

    
    
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo Util::caminho_projeto() ?>/admin/">Área Restrita</a>
        </div>
        
      </div>
    </div>
    



    <?php 
    if(isset($_POST['email']))
    {
        if($obj_login->recupera_senha($_POST['email']) == true)
        {
            Util::alert_bootstrap("Sua nova senha foi enviado para o e-mail cadastrado."); // se nao retornou mostra isso
            Util::script_location("./");
            //header("location: index.php");    
        }
        else
        {
            Util::alert_bootstrap("Não foi possível recuperar sua senha. E-mail não encontrado.");
        }
        
    }
    ?>
    
    
    
    <div class="container-fluid">

      <form class="form-signin" role="form" action="" method="post">
        <h2 class="form-signin-heading">Recuperar senha</h2>
        <input type="email" class="form-control" name="email" placeholder="Email" required autofocus>
        
        
        <button class="btn btn-lg btn-primary btn-block" type="submit">Enviar</button>
        
        <br />
        <p>Informe o dado solicitado e clique em enviar para receber uma nova senha.</p>
       
      </form>

    </div> <!-- /container -->


  



<?php require_once('./includes/rodape.php'); ?>


</body>
</html>