<?php
ob_start();
session_start();

class Produto_Model extends Dao
{

	private $nome_tabela = "tb_produtos";
	private $chave_tabela = "idproduto";
	public $obj_imagem;


	/*	==================================================================================================================	*/
	#	CONSTRUTOR DA CLASSE
	/*	==================================================================================================================	*/
	public function __construct()
	{
            $this->obj_imagem = new Imagem();
            parent::__construct();

	}


	/*	==================================================================================================================	*/
	/*	ORDENA	*/
	/*	==================================================================================================================	*/
	public function atualizar_ordem($ordem){

            if(count($ordem) > 0):

                foreach($ordem as $key=>$orde):

                    $sql = "UPDATE ". $this->nome_tabela ." SET ordem = '$orde' WHERE ". $this->chave_tabela ." = $key ";
                    parent::executaSQL($sql);

                endforeach;

            endif;

	}



    /*  ==================================================================================================================  */
    /*  VERIFICA SE A CATEGORIA ESTÁ SELECIONADA  */
    /*  ==================================================================================================================  */
    public function verifica_categoria_produto($id_categoriaproduto, $idproduto){
        $sql = "SELECT * FROM tb_produtos_categorias WHERE id_categoriaproduto = '$id_categoriaproduto' and id_produto = '$idproduto'  ";
        $result = parent::executaSQL($sql);

        if (mysql_num_rows($result) > 0) {
            return 'selected';
        }else{
            return '';
        }
    }






	/*	==================================================================================================================	*/
	/*	FORMULARIO	*/
	/*	==================================================================================================================	*/
	public function formulario($dados)
	{
        $obj_jquery = new Biblioteca_Jquery();
        $obj_site = new Site();
        ?>

        <script type="text/javascript" language="javascript">
            jQuery(function($){
                $(".moeda").maskMoney({showSymbol:false, symbol:"R$", decimal:",", thousands:"."});
            });
        </script>

        

        <script type="text/javascript">
                $(document).ready(function(){
                    $('#id_tipoveiculo').change(function(){
                        $('#id_marcaveiculo').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_marcas_veiculos.php?id='+$('#id_tipoveiculo').val());
                    });
                });
        </script>


        <script type="text/javascript">
                $(document).ready(function(){
                    $('#id_marcaveiculo').change(function(){
                        $('#id_modeloveiculo').load('<?php echo Util::caminho_projeto(); ?>/includes/carrega_modelos_veiculos.php?id='+$('#id_marcaveiculo').val());
                    });
                });
        </script>



       
        <div class="col-xs-4 form-group ">
            <label>Tipo de veículo</label>
            <?php Util::cria_select_bd("tb_tipos_veiculos", "idtipoveiculo", "titulo", "id_tipoveiculo", $dados[id_tipoveiculo], "form-control fundo-form1 input100") ?>
        </div>
        

        <div class="col-xs-4 form-group ">
            <label>Marca do veículo</label>

            <?php if(!empty($dados[id_marcaveiculo])): ?>
                <?php 
                $sql = "SELECT * FROM tb_marcas_veiculos WHERE id_tipoveiculo = '$dados[id_tipoveiculo]' ";
                Util::cria_select_bd_com_sql("id_marcaveiculo", $dados[id_marcaveiculo], $sql, "", "form-control fundo-form1 input100")
                ?>
            <?php else: ?>
                <select name="id_marcaveiculo" id="id_marcaveiculo" class="form-control fundo-form1 input100">
                  <option value="">Selecione</option>
                </select>
            <?php endif; ?>

        </div>


        <div class="col-xs-4 form-group ">
            <label>Modelo do veículo</label>

            <?php if(!empty($dados[id_modeloveiculo])): ?>
                <?php 
                $sql = "SELECT * FROM tb_modelos_veiculos WHERE id_marcaveiculo = '$dados[id_marcaveiculo]' ";
                Util::cria_select_bd_com_sql("id_modeloveiculo", $dados[id_modeloveiculo], $sql, "", "form-control fundo-form1 input100")
                ?>
            <?php else: ?>
                <select name="id_modeloveiculo" id="id_modeloveiculo" class="form-control fundo-form1 input100">
                  <option value="">Selecione</option>
                </select>
            <?php endif; ?>

        </div>



        <div class="col-xs-4 form-group ">
            <label>Ano inicial aplicação</label>
            <?php Util::get_anos("ano_inicial", $dados[ano_inicial], "Selecione", "form-control fundo-form1 input100") ?>
        </div>


        <div class="col-xs-4 form-group ">
            <label>Ano final aplicação</label>
            <?php Util::get_anos("ano_final", $dados[ano_final], "Selecione", "form-control fundo-form1 input100") ?>
        </div>


        <div class="col-xs-2 form-group ">
            <label>Código</label>
            <input type="text" name="codigo" value="<?php Util::imprime($dados[codigo]) ?>" class="form-control fundo-form1 input100" >
        </div>

        <div class="col-xs-2 form-group ">
            <label>Garantia <span>Ex. 6 meses ou 2 anos</span> </label>
            <input type="text" name="garantia" value="<?php Util::imprime($dados[garantia]) ?>" class="form-control fundo-form1 input100" >
        </div>







        <div class="col-xs-12 form-group ">
            <label>Título</label>
            <input type="text" name="titulo" value="<?php Util::imprime($dados[titulo]) ?>" class="form-control fundo-form1 input100" >
        </div>





        <div class="col-xs-12 form-group ">
            <label>Imagem da capa</label>
            <?php
            if(!empty($dados[imagem])){
            ?>
                <input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
                <label>Atual:</label>
                <div class="clearfix"></div>
                <img class="col-xs-2" src="<?php echo Util::caminho_projeto(); ?>/uploads/<?php Util::imprime($dados[imagem]) ?>" />
            <?php } else { ?>
                <input type="file" name="imagem" id="imagem" value="<?php Util::imprime($dados[imagem]) ?>" class="form-control fundo-form1 input100" />
            <?php } ?>
        </div>



        <div class="col-xs-12 form-group ">
            <label>Descrição</label>
            <?php $obj_jquery->ckeditor('descricao', $dados[descricao]); ?>
        </div>

        











        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->
        <?php require_once("../includes/google_seo.php"); ?>
        <!-- ======================================================================= -->
        <!-- GOOGLE SEO    -->
        <!-- ======================================================================= -->

        



        <!-- ======================================================================= -->
        <!-- VALIDACAO DO FORMULARIO    -->
        <!-- ======================================================================= -->
        <script>
          $(document).ready(function() {
            $('.FormPrincipal').bootstrapValidator({
              message: 'This value is not valid',
              feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
              fields: {


              titulo: {
                validators: {
                  notEmpty: {

                  }
                }
              }, 

              id_tipoveiculo: {
                validators: {
                  notEmpty: {

                  }
                }
              }, 


              id_marcaveiculo: {
                validators: {
                  notEmpty: {

                  }
                }
              },  

            id_modeloveiculo: {
                validators: {
                  notEmpty: {

                  }
                }
              },  

              ano_inicial: {
                validators: {
                  notEmpty: {

                  }
                }
              },  

              ano_final: {
                validators: {
                  notEmpty: {

                  }
                }
              },  

              garantia: {
                validators: {
                  notEmpty: {

                  }
                }
              },  


              valor: {
                validators: {
                  notEmpty: {

                  },
                  numeric: {
                        transformer: function($field, validatorName, validator) {
                            var value = $field.val();
                            return value.replace(',', '');
                        }
                    }
                }
              },  
              

              email: {
                validators: {
                  notEmpty: {

                  },
                  emailAddress: {
                    message: 'Esse endereço de email não é válido'
                  }
                }
              },
              
              mensagem: {
                validators: {
                  notEmpty: {

                  }
                }
              }
            }
          });
          });
        </script>        




            <?php
	}




	/*	==================================================================================================================	*/
	/*	EFETUA O CADASTRO	*/
	/*	==================================================================================================================	*/
	public function cadastra($dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");

                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 236, 212);
            }

            //	CADASTRA O USUARIO
            $id = parent::insert($this->nome_tabela, $dados);


            //  CADASTRA AS CATEGORIAS
            //$this->cadastra_categorias($id, $dados[categoriaproduto]);


            Util::script_msg("Cadastro efetuado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME'])."/cadastra.php");

	}




	/*	==================================================================================================================	*/
	/*	EFETUA A ALTERACAO	*/
	/*	==================================================================================================================	*/
	public function altera($id, $dados)
	{
            //	VERIFICO SE E PARA CRIAR A IMAGEM DA CAPA
            if($_FILES[imagem][name] != "")
            {
                //	EFETUO O UPLOAD DA IMAGEM
                $dados[imagem] = Util::upload_imagem("../../uploads", $_FILES[imagem], "4194304");


                //  CRIO O CROP
                $this->efetua_crop_imagem($dados[imagem], 236, 212);
            }

            parent::update($this->nome_tabela, $id, $dados);

            //  CADASTRA AS CATEGORIAS
           // $this->cadastra_categorias($id, $dados[categoriaproduto]);


           

            Util::script_msg("Alterado com sucesso.");
            Util::script_location(dirname($_SERVER['SCRIPT_NAME']));

	}




    /*  ==================================================================================================================  */
    //  CADASTRA AS CATEGORIAS
    /*  ==================================================================================================================  */
    public function cadastra_categorias($id, $categoriaproduto){
        $sql = "DELETE FROM tb_produtos_categorias WHERE id_produto = '$id' ";
        parent::executaSQL($sql);


        if( count($categoriaproduto) > 0){
            foreach ($categoriaproduto as $key => $categoria) {
                $sql = "INSERT INTO tb_produtos_categorias (id_produto, id_categoriaproduto) VALUES ('$id', '$categoria')";
                parent::executaSQL($sql);
            }
        }


    }





	/*	==================================================================================================================	*/
	/*	ATIVA OU DESATIVA	*/
	/*	==================================================================================================================	*/
	public function ativar_desativar($id, $ativo)
	{
            if($ativo == "SIM")
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'NAO' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "DESATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
            else
            {
                $sql = "UPDATE " . $this->nome_tabela. " SET ativo = 'SIM' WHERE " . $this->chave_tabela. " = '$id'";
                parent::executaSQL($sql);

                //	ARMAZENA O LOG
                parent::armazena_log("tb_logs_logins", "ATIVOU O LOGIN $id", $sql, $_SESSION[login][idlogin]);
            }
	}




	/*	==================================================================================================================	*/
	/*	EXCLUI	*/
	/*	==================================================================================================================	*/
	public function excluir($id)
	{
            //	BUSCA OS DADOS
            $row = $this->select($id);

            $sql = "DELETE FROM " . $this->nome_tabela. " WHERE " . $this->chave_tabela. " = '$id'";
            parent::executaSQL($sql);

            //	ARMAZENA O LOG
            parent::armazena_log("tb_logs_logins", "EXCLUSÃO DO LOGIN $id, NOME: $row[nome], Email: $row[email]", $sql, $_SESSION[login][idlogin]);
	}




	/*	==================================================================================================================	*/
	/*	BUSCA OS DADOS	*/
	/*	==================================================================================================================	*/
	public function select($id = "")
	{
            if($id != "")
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            WHERE
                                    " . $this->chave_tabela. " = '$id'
                            ";
                    return mysql_fetch_array(parent::executaSQL($sql));
            }
            else
            {
                    $sql = "
                            SELECT
                                    *
                            FROM
                                    " . $this->nome_tabela. "
                            ORDER BY
                                    ordem
                            ";
                    return parent::executaSQL($sql);
            }

	}


	/*	==================================================================================================================	*/
	/*	EFETUA CROP DA IMAGEM	*/
	/*	==================================================================================================================	*/
	public function efetua_crop_imagem($nome_arquivo, $largura, $altura, $alias = 'tumb_', $tipo = 'crop')
	{
            $obj_imagem = new m2brimagem("../../uploads/$nome_arquivo");
            $obj_imagem->redimensiona($largura, $altura, $tipo);
            $obj_imagem->grava("../../uploads/".$alias.$nome_arquivo);
	}




}
?>
