<?php
require_once("../../class/Include.class.php");
require_once("Informacao_Model.php");
require_once("../trava.php");

$obj_control = new Informacao_Model();


if(isset($_POST[action]))
{
      $obj_control->altera($_POST[id], $_POST);
      Util::script_msg("Dados alterado com sucesso.");
      Util::script_location("index.php");
}



$_POST = $obj_control->select($_GET[id]);



?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>

<?php require_once("../includes/head.php") ?>

<title>Admin - <?php echo $_SERVER['SERVER_NAME'] ?></title>
</head>

<body>


	<!-- ======================================================================= -->
	<!-- topo	-->
	<!-- ======================================================================= -->
	<?php require_once("../includes/topo.php"); ?>





	<div class="container-fluid">
      <div class="row">


      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-2 sidebar">
      	<?php require_once("../includes/lateral.php"); ?>
      </div>
      <!-- ======================================================================= -->
      <!-- LATERAL	-->
      <!-- ======================================================================= -->





      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->
      <div class="col-xs-10 main ">
            <form action="" class="FormPrincipal" name="FormPrincipal" id="FormPrincipal" method="post" enctype="multipart/form-data">
                
                
               <?php echo $obj_control->formulario_email_smtp($_POST); ?>
                
                
                <div class="text-right col-xs-12" >
                  <input type="hidden" name="id" id="id" value="<?php echo $_GET[id]; ?>" />
                    <input type="hidden" name="action" id="action" value="altera" />
                    
                    <button type="submit" class="btn btn-primary">ENVIAR</button>
                </div>
            </form>
      </div>
      <!-- ======================================================================= -->
      <!-- CONTEUDO PRINCIPAL	-->
      <!-- ======================================================================= -->


        




</body>
</html>